var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix
        .copy('resources/bower_components/jquery/dist/jquery.js', 'public/js/jquery.js')
        .copy('resources/bower_components/bootstrap/dist/js/bootstrap.js', 'public/js/bootstrap.js')
        .copy('resources/bower_components/d3/d3.js', 'public/js/d3.js')
        .copy('resources/bower_components/gsap/src/uncompressed/TweenLite.js', 'public/js/tweenlite.js')
        .copy('resources/bower_components/gsap/src/uncompressed/easing/EasePack.js', 'public/js/easepack.js')
        .copy('resources/bower_components/jquery.viewportchecker.js', 'public/js/jquery.viewportchecker.js')
        .copy('resources/bower_components/svg-injector/svg-injector.js', 'public/js/svg-injector.js')
        .copy('resources/bower_components/slick/slick.js', 'public/js/slick.js')
        .copy('resources/bower_components/slick/slick.css', 'public/css/slick.css')
        .copy('resources/bower_components/slick/slick-theme.css', 'public/css/slick-theme.css')
        .copy('resources/bower_components/svg-morpheus/compile/unminified/svg-morpheus.js', 'public/js/svg-morpheus.js')
        .copy('resources/bower_components/fontawesome/css/font-awesome.css', 'public/css/font-awesome.css')
        .coffee()
        .less()
        .scripts(
        ['jquery.js', 'bootstrap.js', 'd3.js', 'graph.js', 'slick.js', 'tweenlite.js', 'easepack.js', 'animate.js', 'nodes.js', 'jquery.viewportchecker.js', 'app.js'],
        'public/js/all.js', 'public/js')
        .styles(['app.css', 'slick.css', 'slick-theme.css', 'font-awesome.css'], 'public/css/all.css', 'public/css')
    ;
});
