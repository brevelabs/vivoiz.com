$('.slick').slick({
    autoplay: true,
    autoplaySpeed: 4000,
    arrows: false,
    dots: true,
    infinite: true
})

$slick = $('#character-slick').slick({
    autoplay: false,
    autoplaySpeed: 4000,
    arrows: false,
    draggable: false,
    dots: true,
    fade: true,
    easing: 'easeInOut',
    pauseOnHover: false,
    speed: 1000
}).on('beforeChange', (event, sl, cur) ->
    if cur is 3
        $slick.slickPause()
        window.setTimeout(
            () ->
                $slick.slickPlay()
        , 5000
        )
).slick('getSlick')

sliders = false
if not /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
    sliders = true
    $('#more').viewportChecker({
        callbackFunction: () ->
            window.setTimeout(
                () ->
                    $slick.slickPlay()
            , 500
            )
        ,
        offset: 100,
        invertBottomOffset: true,
        repeat: false,
    })
    $('.circle').viewportChecker({
        classToAdd: 'animated',
        classToRemove: '',
        offset: 100,
        invertBottomOffset: true,
        repeat: false,
        callbackFunction: (elem, action) ->,
        scrollHorizontal: false
    })
else
    window.setTimeout(
        () ->
            $slick.slickPlay()
    , 2000
    )
    $('.circle').css('visibility', 'visible')

if not sliders
    window.setTimeout(
        () ->
            $slick.slickPlay()
    , 2000
    )
    $('.circle').css('visibility', 'visible')

$('a[href*=#]:not([href=#])').click(
    () ->
        if location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//,
            '') and location.hostname == this.hostname
            target = $(this.hash)
        if target.length
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000)
            false
)

throttle = (fn, delay) ->
    return fn if delay is 0
    timer = false
    return ->
        return if timer
        timer = true
        setTimeout (-> timer = false), delay unless delay is -1
        fn arguments...


$(window).scroll(() ->
    st = $(window).scrollTop()
    h = $('#graph').height()
    if st > h
        $('#navbar').fadeIn("slow")
    else
        $('#navbar').fadeOut("slow")
    null
)