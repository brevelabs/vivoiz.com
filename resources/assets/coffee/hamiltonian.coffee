hamilCount = 0
width = window.innerWidth
height = window.innerHeight
hamil = (stepsArray = [], numRepeats = 1, rcx = 0, rcy = 0) ->
    this.init = () ->
        radius = 240
        numVertices = numRepeats * stepsArray.length
        numSteps = stepsArray.length
        colors = d3.scale.category10().range()
        rcx += width / 2
        rcy += height / 2
        nodes = [] # the node with index 0 is fixed to the center and has a high charge
        links = []
        edges = []
        animationSpeed = 300
        currentFrame = 0
        charge = 100
        interval = null

        currentVertex = 0
        currentVertexOffset = 0

        force = d3.layout.force().charge((d, i) ->
            c = 0
            if not (i is 0)
                c = -charge
            c
        ).size([rcx * 2, height]).linkStrength(0.1).friction(0.9).gravity(0.1).theta(0.8).alpha(0.1)
        svg = d3.select('#hamil').append('svg').attr('width', width).attr('height', height).attr('id',
            "hamil#{hamilCount++}")

        dupCache = {}
        for i in [0..numVertices - 1]
            source = i % numVertices
            target = (numVertices + i + stepsArray[i % numSteps]) % numVertices
            hp_target = (source + 1) % numVertices
            if target < 0
                target = numVertices + target
            if not dupCache["#{source},#{hp_target}"] and not dupCache["#{hp_target},#{source}"]
                edges.push({source: source, target: hp_target});
                dupCache["#{source},#{hp_target}"] = 1

            if not dupCache["#{source},#{target}"] and not dupCache["#{target},#{source}"]
                edges.push({source: source, target: target})
                dupCache[source + ',' + target] = 1

        resize = () ->
            null

        drawCircles = () ->
            svg.selectAll('circle').data(nodes).enter().append('svg:circle')
            .attr("r", (d, i) ->
                if i is 0
                    0
                else
                    10
            )
            .attr("class", (d, i) ->
                if i is 0
                    'magic-vertex'
                else
                    'vertex'
            )
            .attr("fixed", (d, i) ->
                if i is 0
                    'true'
                else
                    'false'
            )
            .attr("cx", (d) -> d.x)
            .attr("cy", (d) -> d.y)


        drawLines = () ->
            svg.selectAll("line.link")
            .data(links)
            .enter()
            .append("svg:line")
            .attr("class", "link")
            .attr("x1", (d) -> d.source.x)
            .attr("y1", (d) -> d.source.y)
            .attr("x2", (d) -> d.target.x)
            .attr("y2", (d) -> d.target.y)
        # .exit()
        # .remove()

        redraw = () ->
            currentFrame = 1

        animate = () ->
            stepInstruction = stepsArray[currentVertex % stepsArray.length]
            currentStep = (currentVertex + currentVertexOffset) - numVertices * Math.floor((currentVertex + currentVertexOffset) / numVertices)
            circles = svg.selectAll(".vertex")
            if currentVertexOffset is 0
                circles.filter((d, i) -> currentVertex == i).style("fill", colors[2])
                circles.filter((d, i) -> currentVertex > i).style("fill", colors[0])
                circles.filter((d, i) -> currentVertex < i).style("fill", "white")
                if stepInstruction > 0
                    currentVertexOffset += 1
                else
                    currentVertexOffset -= 1
            else if not (currentVertexOffset is stepInstruction)
                circles.filter((d, i) -> currentStep is i).style("fill", colors[1])
                if stepInstruction > 0
                    currentVertexOffset += 1
                else
                    currentVertexOffset -= 1
            else if currentVertex < numVertices
                circles.filter((d, i) -> currentStep == i).style("fill", colors[1])

                links.push({source: nodes.slice(1)[currentVertex], target: nodes.slice(1)[currentStep]});
                drawLines()
                force.links(links)
                force.start()
                currentVertex++
                currentVertexOffset = 0
            if currentVertex is numVertices
                clearInterval(interval)
                interval = null
                svg.selectAll(".vertex").style("fill", colors[0])
            else
                interval = setTimeout(
                    () -> animate()
                , animationSpeed)

        draw = () ->
            clearInterval(interval)
            nodes = [{fixed: true, x: rcx, y: rcy, origX: rcx, origY: rcy}]
            nodes = nodes.concat(d3.range(numVertices).map((d, i) ->
                x = rcx + radius * Math.cos((i * 2 * Math.PI / numVertices) - Math.PI / 2)
                y = rcy + radius * Math.sin((i * 2 * Math.PI / numVertices) - Math.PI / 2)
                {
                    x: x, origX: x, y: y, origY: y
                }
            ))
            force.nodes(nodes)
            force.start()
            nodes.slice(1).forEach(
                (target, i) ->
                    s = null
                    if i is (nodes.length - 2)
                        s = nodes[1]
                    else
                        s = nodes[i + 2]
                    links.push(
                        {
                            source: s,
                            target: target,
                            linkDistance: 0
                        }
                    )
            )
            svg.selectAll(".vertex").style("fill", "white")
            drawLines()
            interval = setInterval(animate, animationSpeed)
            drawCircles()
            svg.selectAll('circle').call(force.drag)

        timer = null
        force.on('tick', () ->
            if currentVertex >= numVertices and not timer
                timer = setTimeout(
                    () ->
                        d = nodes[randInt(1, nodes.length - 1)]
                        d.x = d.px + random(-10, 10)
                        d.y = d.py + random(-10, 10)
                        timer = null
                        force.resume()
                , 500
                )
            svg.selectAll("circle")
            .attr("cx", (d) -> d.x)
            .attr("cy", (d) -> d.y)
            .transition().ease('linear')

            svg.selectAll("line.link")
            .attr("x1", (d) -> d.source.x)
            .attr("y1", (d) -> d.source.y)
            .attr("x2", (d) -> d.target.x)
            .attr("y2", (d) -> d.target.y)
            .transition().ease('linear')

            currentFrame++
        )

        draw()

        throttle = (fn, delay) ->
            return fn if delay is 0
            timer = false
            return ->
                return if timer
                timer = true
                setTimeout (-> timer = false), delay unless delay is -1
                fn arguments...

        window.addEventListener('resize', throttle(
                () ->
                    redraw()
            )
        )
    this
randInt = (min, max) ->
    Math.floor(Math.random() * (max - min)) + min

random = (min, max) ->
    Math.random() * (max - min) + min

(new hamil([2, 6, -2], 4, randInt(-width / 2 + 100, -100), randInt(-200, 200))).init()
(new hamil([2, 6, -2], 4, randInt(100, width / 2 - 100), randInt(-200, 200))).init()
null