lastTime = 0
vendors = ['ms', 'moz', 'webkit', 'o']
if not window.requestAnimationFrame
    for vendor in vendors
        window.requestAnimationFrame = window[vendor + 'RequestAnimationFrame']
        window.cancelAnimationFrame = window[vendor + 'CancelAnimationFrame'] || window[vendor + 'CancelRequestAnimationFrame']
    null

if  not window.requestAnimationFrame
    window.requestAnimationFrame = (callback) ->
        currTime = (new Date()).getTime()
        timeToCall = Math.max(0, 16 - (currTime - lastTime))
        id = window.setTimeout(
            () -> callback(currTime + timeToCall),
            timeToCall
        )
        lastTime = currTime + timeToCall;
        id

if not window.cancelAnimationFrame
    window.cancelAnimationFrame = (id) ->
        clearTimeout(id)
        null