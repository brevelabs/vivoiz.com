drawGraph = () ->
    count = 1
    width = window.innerWidth
    height = window.innerHeight
    n = 4 # number of layers
    m = Math.max(5, Math.floor(width / 250)) # number of samples per layer
    colors = ['rgba(250, 138, 47, 0.95)', 'rgba(194, 5, 69, 0.95)', 'rgba(250, 72, 51, 0.95)',
              'rgba(39, 148, 151, 0.95)']

    randInt = (min = 0, max = 255) ->
        Math.floor(Math.random() * (max - min) + min)

    randomColor = () ->
        "rgba(#{randInt()}, #{randInt()}, #{randInt()}, #{randInt(90, 100) * 0.01})"

    rand = (min, max, above) ->
        mid = (min + max) / 2
        num = 0
        if above is 0
            num = Math.random() * (max - mid) + mid
        else
            num = Math.random() * (mid - min) + min
        num

    layers = []
    scaleX = d3.scale.linear().domain([0, m - 1]).range([0, width])
    createLayers = (n, m, zero = false) =>
        # Create layer: set data points and use color code for colors array
        h = Math.min(height, width)
        layer = (id, k) =>
            _height = h / 3 + 2 / 3 * h / n * (n - id)
            _slope = _height / width
            points = for i in [0..k - 1]
                x = scaleX(i)
                ret = {x: x, y: 0}
                if not zero and i < k
                    ret = {x: x, y: rand(0, _slope * x, (count + id + i) % 2)}
                ret
            {color: colors[id] || randomColor(), values: points}
        # Create requested number of layers
        layers = (layer(i, m) for i in [0..n - 1])
        # return layers object
        layers

    svgTransformX = (x) -> x
    svgTransformY = (y) -> height - y

    stack = d3.layout.stack().offset("wiggle").values((d) -> d.values)
    # jQuery
    svg = d3.select('#graph').insert("svg", ':first-child').attr("width", width).attr("height", height)
    area = d3.svg.area().x((d) -> svgTransformX(d.x)).y0(() -> svgTransformY(0)).y1((d) -> svgTransformY(d.y)).interpolate("cardinal")

    svg.selectAll("path").data(stack(createLayers(n, m, true)))
    .enter().append("path").attr("d", (d) -> area(d.values)).style("fill", (d) -> d.color)
    svg.selectAll("path").data(stack(createLayers(n, m))).transition().duration(2500).attr('d',
        (d) -> area(d.values))
    transition = () ->
        if document.body.scrollTop < height
            svg.selectAll("path").data(stack(createLayers(n, m))).transition().duration(3000).attr('d',
                (d) -> area(d.values))
        window.setTimeout(transition, 4000)
        count++

    throttle = (fn, delay) ->
        return fn if delay is 0
        timer = false
        return ->
            return if timer
            timer = true
            setTimeout (-> timer = false), delay unless delay is -1
            fn arguments...

    window.addEventListener('resize', throttle(
            () ->
                $('#graph svg').remove()
                drawGraph()
        )
    )

    transition()
    null
drawGraph()