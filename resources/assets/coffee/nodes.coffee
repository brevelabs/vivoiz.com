largeHeader = document.getElementById 'nodes'
primaryColor = 'rgba(255,255,255,'
width = $(largeHeader).innerWidth()
height = $(largeHeader).innerHeight()

# Not part of nodes
$elem = $(largeHeader).find('> div')
$elem.css('margin-top', - 250 - $elem.height() / 2)

target = {x: width / 2, y: height / 2}
animateHeader = true

scrollTopThreshold = $('#nodes').offset().top

canvas = document.createElement('canvas')

$(largeHeader).prepend(canvas)
canvas.width = width
canvas.height = height
ctx = canvas.getContext('2d')

animate = () ->
    if animateHeader
        ctx.clearRect(0, 0, width, height);
        for point in points
            if Math.abs(getDistance(target, point)) < 3000
                point.active = 0.3
                point.circle.active = 0.6
            else if Math.abs(getDistance(target, point)) < 20000
                point.active = 0.1
                point.circle.active = 0.3
            else if Math.abs(getDistance(target, point)) < 40000
                point.active = 0.02
                point.circle.active = 0.1
            else
                point.active = 0;
                point.circle.active = 0
            drawLines(point)
            point.circle.draw()
    requestAnimationFrame animate
    null

shiftPoint = (p) ->
    TweenLite.to(
        p,
        rand(1, 4),
        {
            x: p.originX + rand(-50, 50),
            y: p.originY + rand(-50, 50),
            ease: Ease.easeInOut,
            onComplete: () -> shiftPoint(p)
        })

drawLines = (p) ->
    if p.active > 0
        for point in p.closest
            ctx.beginPath()
            ctx.moveTo(p.x, p.y)
            ctx.lineTo(point.x, point.y)
            ctx.strokeStyle = primaryColor + p.active + ')'
            ctx.stroke()


Circle = (pos, rad, color) ->
    self = this
    self.pos = pos
    self.radius = rad
    self.color = color

    self.draw = () ->
        if self.active
            ctx.beginPath()
            ctx.arc(self.pos.x, self.pos.y, self.radius, 0, 2 * Math.PI, false)
            ctx.fillStyle = primaryColor + self.active + ')';
            ctx.fill()
    self

getDistance = (p1, p2) ->
    Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2)

rand = (min, max) ->
    Math.floor(Math.random() * (max - min) + min)

mouseMove = (event) ->
    x = 0
    y = 0
    if event.pageX || event.pageY
        x = event.pageX
        y = event.pageY
    else if event.clientX || event.clientY
        x = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft
        y = event.clientY + document.body.scrollTop + document.documentElement.scrollTop
    target.x = x
    target.y = y - scrollTopThreshold
    true

scrollCheck = () ->
    scrollTopThreshold = $('#nodes').offset().top
    if document.body.scrollTop > scrollTopThreshold - height and document.body.scrollTop < scrollTopThreshold + height
        animateHeader = true
    else
        animateHeader = true

throttle = (fn, delay) ->
    return fn if delay is 0
    timer = false
    return ->
        return if timer
        timer = true
        setTimeout (-> timer = false), delay unless delay is -1
        fn arguments...

points = []
dx = width / 20
dy = height / 20
for x in [0..width] by dx
    for  y in [0..height] by dy
        px = x + rand(0, dx)
        py = y + rand(0, dy)
        points.push {x: px, originX: px, y: py, originY: py}

for p1 in points
    closest = [];
    for p2 in points
        if not (p1 is p2)
            placed = false
            for i in [0..4]
                if not placed
                    if not closest[i]
                        closest[i] = p2
                        placed = true

            for i in [0..4]
                if not placed
                    if getDistance(p1, p2) < getDistance(p1, closest[i])
                        closest[i] = p2
                        placed = true
    p1.closest = closest

for point in points
    point.circle = new Circle(point, rand(2, 4), primaryColor + '0.3)')

if not /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
    window.addEventListener('mousemove', mouseMove)
    window.addEventListener('scroll', throttle(scrollCheck, 50))
    animate()
    for point in points
        shiftPoint(point)
else
    ctx.clearRect(0, 0, width, height)